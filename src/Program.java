import org.scilab.forge.jlatexmath.ParseException;
import org.scilab.forge.jlatexmath.TeXConstants;
import org.scilab.forge.jlatexmath.TeXFormula;
import org.scilab.forge.jlatexmath.TeXIcon;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.regex.*;

public class Program implements Runnable {

    private TrayIcon trayIcon = null;
    private JPopupMenu trayMenu = null;
    private JMenuItem exitItem = null;
    private JMenuItem settingsItem = null;
    private Timer timer;
    private ClipboardChecker clipboardChecker = new ClipboardChecker();
    private Pattern latexRegexPattern = Pattern.compile("!!LATEX(.*)");
    private JDialog settingsDialog = null;
    private JTextField regexField = null;
    private JButton okButton = null;
    private JLabel regexLabel = null;
    private GridLayout settingsLayout = null;

    public static void main(String[] args) {
        Program program = new Program();
        javax.swing.SwingUtilities.invokeLater(program);
    }


    public Program() {
        initGui();
    }

    @Override
    public void run() {
        ActionListener onTimer = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clipboardChecker.pollClipboard();
            }
        };
        clipboardChecker.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (latexRegexPattern.matcher(e.getActionCommand()).matches()) {
                    Matcher matcher = latexRegexPattern.matcher(e.getActionCommand());
                    String data;
                    if (matcher.find()) {
                        data = matcher.group(1);
                    }
                    else {
                        return;
                    }
                    TeXFormula formula;
                    try {
                        formula = new TeXFormula(data);

                    } catch (ParseException ex) {
                        trayIcon.displayMessage("Error", "Could not parse LaTeX input", TrayIcon.MessageType.ERROR);
                        return;
                    }
                    TeXIcon ti = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, 40);
                    ti.setInsets(new Insets(5, 5, 5, 5));
                    BufferedImage image = new BufferedImage(ti.getIconWidth(), ti.getIconHeight(), BufferedImage.TYPE_4BYTE_ABGR);
                    image.getGraphics().setColor(Color.WHITE);
                    image.getGraphics().fillRect(0, 0, ti.getIconWidth(), ti.getIconHeight());
                    ti.paintIcon(new JLabel(), image.getGraphics(), 0, 0);
                    TransferableImage transferableImage = new TransferableImage(image);
                    Toolkit.getDefaultToolkit().getSystemClipboard().setContents(transferableImage, new ClipboardOwner() {
                        @Override
                        public void lostOwnership(Clipboard clipboard, Transferable contents) {

                        }
                    });
                    trayIcon.displayMessage("Success", "Image successfully generated", TrayIcon.MessageType.INFO);
                }
            }
        });
        timer = new Timer(100, onTimer);
        timer.setRepeats(true);
        timer.start();
    }

    private void initGui() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        settingsDialog = new JDialog();
        settingsDialog.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
        settingsDialog.setSize(200, 100);
        settingsDialog.setLocationRelativeTo(null);
        settingsDialog.setResizable(false);

        regexField = new JTextField();
        regexField.setText(latexRegexPattern.toString());
        regexField.setFont(new Font("Courier New", Font.PLAIN, 12));

        regexLabel = new JLabel("Enter regex:");

        settingsLayout = new GridLayout(2, 2, 10, 20);
        settingsDialog.getContentPane().setLayout(settingsLayout);

        okButton = new JButton("OK");
        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                latexRegexPattern = Pattern.compile(regexField.getText());
                settingsDialog.setVisible(false);
            }
        });

        settingsDialog.getContentPane().add(regexLabel);
        settingsDialog.getContentPane().add(regexField);
        settingsDialog.getContentPane().add(okButton);

        BufferedImage iconImage;
        try {
            InputStream stream = Program.class.getResourceAsStream("/joj.png");
            iconImage = ImageIO.read(stream);
        } catch (IOException e) {
            iconImage = new BufferedImage(32, 32, BufferedImage.TYPE_3BYTE_BGR);
        }

        settingsDialog.setIconImage(iconImage);

        trayIcon = new TrayIcon(iconImage);
        trayIcon.setImageAutoSize(true);

        trayMenu = new JPopupMenu();

        exitItem = new JMenuItem("Quit");
        exitItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        settingsItem = new JMenuItem("Settings...");
        settingsItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                settingsDialog.setVisible(true);
            }
        });

        trayMenu.add(settingsItem);
        trayMenu.addSeparator();
        trayMenu.add(exitItem);

        try {
            SystemTray.getSystemTray().add(trayIcon);
        } catch (AWTException e) {
            e.printStackTrace();
        }

        trayIcon.addMouseListener(new MouseListener() {
            public void mouseClicked(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON3) {
                    trayMenu.setLocation(e.getX(), e.getY());
                    trayMenu.setInvoker(trayMenu);
                    trayMenu.setVisible(true);
                }
                else if (e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() == 2)
                {
                    settingsDialog.setVisible(true);
                }
            }

            public void mousePressed(MouseEvent e) {

            }

            public void mouseReleased(MouseEvent e) {

            }

            public void mouseEntered(MouseEvent e) {

            }

            public void mouseExited(MouseEvent e) {

            }
        });

        trayIcon.displayMessage("LazyClipboardLaTeX loaded!", "Right click the icon for additional options", TrayIcon.MessageType.NONE);
    }

}