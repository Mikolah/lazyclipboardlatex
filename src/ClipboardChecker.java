import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ClipboardChecker {
    static private Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
    private final List<ActionListener> actionListeners = new ArrayList<ActionListener>();


    public void addActionListener(ActionListener newListener) {
        actionListeners.add(newListener);
    }

    public void pollClipboard() {
        String newData;
        try {
            newData = (String) clipboard.getData(DataFlavor.stringFlavor);
        } catch (UnsupportedFlavorException e) {
            newData = "";
        } catch (IOException e) {
            return;
        } catch (IllegalStateException e) {
            return;
        }

        if (!newData.equals(currentData)) {
            currentData = newData;
            ActionEvent e = new ActionEvent(this, ActionEvent.ACTION_PERFORMED, currentData);
            for (ActionListener l: actionListeners) {
                l.actionPerformed(e);
            }
        }
    }

    private String currentData = "";

}
